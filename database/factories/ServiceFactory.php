<?php

use Faker\Generator as Faker;
use App\Service;

$factory->define(Service::class, function (Faker $faker) {
    return [ 
        'title' => $faker->word(),
        'description' => $faker->sentence(8),
        'price' => rand(5000,65535)
    ];
});
