<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('flowOrder')->nullable();
            $table->date('appointment_date')->nullable();
            $table->tinyInteger('appointment_turn')->nullable();
            $table->unsignedInteger('service_id');
            $table->unsignedInteger('client_id');
            $table->date('spell_date')->nullable();
            $table->boolean('spell_finished')->default(0);
            
            $table->foreign('appointment_date')->references('date')->on('schedule_dates')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('appointments');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
