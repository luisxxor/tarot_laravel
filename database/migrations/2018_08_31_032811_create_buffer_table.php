<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBufferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buffer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('anónimo');
            $table->string('lastname')->nullable();
            $table->string('email');
            $table->string('tel')->nullable();
            $table->date('appointment_date')->nullable();
            $table->tinyInteger('appointment_turn')->nullable();
            $table->unsignedInteger('service_id');
            $table->foreign('service_id')->references('id')->on('services');
            $table->string('payment_url')->nullable();
            $table->string('token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('buffer');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
