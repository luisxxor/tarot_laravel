<?php

use Illuminate\Database\Seeder;

class DefaultDatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$normalday = "[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true]";

    	$weekend = "[true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true]";

    	DB::table('default_dates')->insert([
            'weekday' => 0,
        	'day_data' => $normalday
        ]);
        DB::table('default_dates')->insert([
            'weekday' => 1,
        	'day_data' => $normalday
        ]);
        DB::table('default_dates')->insert([
            'weekday' => 2,
        	'day_data' => $normalday
        ]);
        DB::table('default_dates')->insert([
            'weekday' => 3,
        	'day_data' => $normalday
        ]);
        DB::table('default_dates')->insert([
            'weekday' => 4,
        	'day_data' => $normalday
        ]);
        DB::table('default_dates')->insert([
            'weekday' => 5,
        	'day_data' => $weekend
        ]);
        DB::table('default_dates')->insert([
            'weekday' => 6,
        	'day_data' => $weekend
        ]);
    }
}
