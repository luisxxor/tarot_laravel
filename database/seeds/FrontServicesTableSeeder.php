<?php

use Illuminate\Database\Seeder;

class FrontServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('front_services')->insert([
            'title' => 'Tarot de Marsella',
            'description' => 'El Tarot de Marsella (en francés Tarot de Marseille), juego que utiliza la baraja de cartas del Tarot más conocida y de la cual derivan todas las posteriores.',
            'image_name' => 'tarot_marsella.jpg',
            'priority' => 100
        ]);

        DB::table('front_services')->insert([
            'title' => 'Tarot de los Ovnis',
            'description' => 'Un tarot moderno, para una lectura mas que moderna. Veras que los mensajeros de otros mundos nos llevan a descubrirnos a nosotros mismos, y nos invitará a mirar en los rincones más ocultos del alma humana.',
            'image_name' => 'tarot_ovni.jpg',
            'priority' => 100
        ]);

        DB::table('front_services')->insert([
            'title' => 'Tarot de los Angeles',
            'description' => 'Tarot de los ángeles. ¿A veces desearías que alguien estuviera velando por ti en cada momento? Deseo concedido, ¡tales son tus ángeles de la guarda!.',
            'image_name' => 'tarot_angeles.jpg',
            'priority' => 100
        ]);

        DB::table('front_services')->insert([
            'title' => 'Tarot de las Brujas',
            'description' => 'Una maravillosa obra de arte, reflejada en estas impresionantes ilustraciones a todo color con un estilo digital en perfecta armonía en cada uno de sus 78 arcanos del tarot. Un tarot que nos muestra la magia y la brujería en todo su esplendor y poder con toques románticos y medievales con un sinfín de detalles preciosos en toda su historia.',
            'image_name' => 'tarot_brujas.jpg',
            'priority' => 100
        ]);

        DB::table('front_services')->insert([
            'title' => 'Tarot Egipcio',
            'description' => 'Es considerada como una de las barajas sagradas y la clave de todas las potencias y dogmas, desde el punto de vista del esoterismo y se tiene como la más precisa.',
            'image_name' => 'tarot_egipcio.jpg',
            'priority' => 100
        ]);

        DB::table('front_services')->insert([
            'title' => 'Consulta por otros mazos',
            'image_name' => 'tarot_plus.jpg',
            'priority' => 100
        ]);
    }
}
