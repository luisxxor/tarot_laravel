<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        $this->callSeeders([
        	ServicesTableSeeder::class,
            DefaultDatesTableSeeder::class,
            ScheduleDatesTableSeeder::class,
            AppointmentsTableSeeder::class,
            UsersTableSeeder::class,
            FrontServicesTableSeeder::class
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    private function callSeeders(array $classes)
    {
    	foreach ($classes as $class) {
    		$this->call($class);
    	}
    }
}
