<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use App\Service;

class AppointmentModuleTest extends TestCase
{

	use RefreshDatabase;
    /** @test */
    public function an_user_can_create_one_appointment()
    {

		$service = factory(Service::class)->create();
		DB::table('default_dates')->insert([
            'weekday' => 3,
        	'day_data' => "[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true]"
        ]);

    	$hi = $this->withHeaders(['Accept'=>'application/json','Content-Type' => 'application/json'])
    	->json('POST', '/api/appointment/create', [
    		'client_name' => 'Luis',
    		'client_lastname' => 'Portillo',
    		'appointment_date' => '2018-08-09',
    		'appointment_turn' => '38',
    		'service_id' => 1
		])->assertStatus(201);
    }
}
