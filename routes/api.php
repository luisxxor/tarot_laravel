<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'prefix' => 'auth'

], function ($router) {

    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([
    'prefix' => 'auth'
],function($router) {
    Route::post('login', 'AuthController@login');
});

Route::post('/flow/payment','Flow@generatePayment');
Route::delete('/buffer/{id}','BufferController@destroy');
Route::post('/buffer/create','BufferController@store');
Route::get('/buffer/{id}','BufferController@index');
Route::get('/services/{id?}','ServiceController@index')->where('id', '[0-9]+');
Route::get('/services/select','ServiceController@select');
Route::post('/appointment/create','AppointmentController@store');
Route::get('/appointment/available/{date}','AppointmentController@available');
Route::get('/front_services/{id?}','FrontServiceController@index')->where('id', '[0-9]+');
Route::post('/appointment/onCreate','AppointmentController@onCreateSendMail');
Route::get('/clients/select','ClientController@select');

Route::group([
    'middleware' => 'jwt.auth'
], function ($router) {
    Route::post('/services','ServiceController@store');
    Route::put('/services/{id}','ServiceController@update')->where('id','[0-9]+');
    Route::get('/services/non-agendable','ServiceController@getNonAgendable');
    Route::delete('/services/{id}','ServiceController@destroy');
    Route::get('/appointment/admin-available/{date}','AppointmentController@adminAvailable');
    Route::get('/appointment/admin-select-available/{date}/{selected_turn?}','AppointmentController@adminSelectAvailable')->where(['date' => '[0-9]{4}\-[0-9]{2}\-[0-9]{2}','selected_turn' => '[0-9]{1,2}']);
    Route::post('/defaultdate/','DefaultDateController@update');
    Route::get('/defaultdate/','DefaultDateController@index');
    Route::get('/users/{id?}','UserController@index')->where('id', '[0-9]+');
    Route::post('/users','UserController@store');
    Route::post('/user/available','UserController@usernameAvailable');
    Route::put('/users/{id}','UserController@update');
    Route::delete('/users/{id}','UserController@destroy');
    Route::get('/appointments/resume','AppointmentController@resume');
    Route::get('/appointments/nextSpells','AppointmentController@nextSpells');
    Route::get('/appointments/{id?}','AppointmentController@index')->where('id', '[0-9]+');
    Route::delete('/appointments/{id}','AppointmentController@destroy');
    Route::post('/appointment/admin-create','AppointmentController@adminStore');
    Route::put('/appointment/{id}','AppointmentController@update');
    Route::post('/front_services/','FrontServiceController@store');
    Route::put('/front_services/{id}','FrontServiceController@update');
    Route::delete('/front_services/{id}','FrontServiceController@destroy');
    Route::put('/landing_image/','landingImageController@update');
    Route::get('/clients/{id?}','ClientController@index')->where('id','[0-9]+');
    Route::post('/clients','ClientController@store');
    Route::put('/clients/{id}','ClientController@update');
    Route::delete('/clients/{id}','ClientController@destroy');
    Route::get('/appointment/getToday','AppointmentController@getToday');
});