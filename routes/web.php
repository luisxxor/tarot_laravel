<?php

use Illuminate\Support\Facades\Request;
use App\Mail\PaymentDone;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/payment_status','Flow@getPaymentStatus');
Route::post('/payment_confirm','Flow@confirmPayment');

Route::group(['prefix' => 'admin'], function () {
    Route::get('{any?}', function () {
        return view('admin');
    })->where('any', '.*');
});

Route::get('{any?}',function(){
	return view('welcome');
})->where('any', '^((?!mail).)*')->name('anything');
