export function login(credentials) {
  return new Promise((res, rej) => {
    axios
      .post("/api/auth/login", credentials)
      .then(response => {
        res(response.data);
      })
      .catch(err => {
        window.error = err;
        console.log(err.response);
        rej(err.response);
      });
  });
}

export function getLocalUser() {
  const userStr = localStorage.getItem("user");

  if (!userStr) {
    return null;
  }

  return JSON.parse(userStr);
}
