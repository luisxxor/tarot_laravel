export function getIndexFromStartTime(time) {
  let hours = parseInt(time.split(":")[0]);
  let minutes = parseInt(time.split(":")[1]);
  if (hours > 23 || hours < 0 || (minutes != 30 && minutes != 0)) {
    return false;
  }

  return hours * 2 + (minutes == 30 ? 1 : 0);
}

export function getStartTimeFromIndex(index) {
  if (index > 48 || index < 0) {
    return false;
  }

  if (index == 48) {
    return "00:00";
  }

  let hours = Math.floor(index / 2).toString();
  if (hours.length == 1) {
    hours = "0" + hours;
  }

  let minutes = index % 2 == 0 ? "00" : "30";

  return hours + ":" + minutes;
}

export function getEndTimeFromIndex(index) {
  if (index > 48 || index < 0) {
    return false;
  }

  if (index == 48) {
    return "00:29";
  }

  let hours = Math.floor(index / 2).toString();
  if (hours.length == 1) {
    hours = "0" + hours;
  }

  let minutes = index % 2 == 0 ? "29" : "59";

  return hours + ":" + minutes;
}

export function getActualTime() {
  let now = new Date();
  let hours = now.getHours() > 9 ? "" + now.getHours() : "0" + now.getHours();
  let minutes =
    now.getMinutes() > 9 ? "" + now.getMinutes() : "0" + now.getMinutes();
  return `${hours}:${minutes}`;
}

export function getNextTurn() {
  let now = new Date();
  now.setHours(23, 50);
  let hours = now.getHours();
  let minutes = now.getMinutes();

  if (minutes < 30) {
    minutes = "30";
  } else {
    minutes = "00";
    if (hours == 23) {
      hours = 23;
    } else {
      hours++;
    }
  }

  hours = hours > 9 ? "" + hours : "0" + hours;
  return `${hours}:${minutes}`;
}

export function getZodiacSign(date) {
  if (date == null) {
    return "";
  }
  let datearray = date.split("-");
  datearray[1]--;
  let dateobj = new Date(...datearray, 0, 0, 0);

  const zod_signs = [
    "Capricornio",
    "Acuario",
    "Piscis",
    "Aries",
    "Tauro",
    "Geminis",
    "Cancer",
    "Leo",
    "Virgo",
    "Libra",
    "Escorpio",
    "Sagitario"
  ];
  let day = dateobj.getDate();
  let month = dateobj.getMonth();
  let zodiacSign = "";
  switch (month) {
    case 0:
      {
        zodiacSign = day < 20 ? zod_signs[0] : zod_signs[1];
      }
      break;
    case 1:
      {
        zodiacSign = day < 19 ? zod_signs[1] : zod_signs[2];
      }
      break;
    case 2:
      {
        zodiacSign = day < 21 ? zod_signs[2] : zod_signs[3];
      }
      break;
    case 3:
      {
        zodiacSign = day < 20 ? zod_signs[3] : zod_signs[4];
      }
      break;
    case 4:
      {
        zodiacSign = day < 21 ? zod_signs[4] : zod_signs[5];
      }
      break;
    case 5:
      {
        zodiacSign = day < 21 ? zod_signs[5] : zod_signs[6];
      }
      break;
    case 6:
      {
        zodiacSign = day < 23 ? zod_signs[6] : zod_signs[7];
      }
      break;
    case 7:
      {
        zodiacSign = day < 23 ? zod_signs[7] : zod_signs[8];
      }
      break;
    case 8:
      {
        zodiacSign = day < 23 ? zod_signs[8] : zod_signs[9];
      }
      break;
    case 9:
      {
        zodiacSign = day < 23 ? zod_signs[9] : zod_signs[10];
      }
      break;
    case 10:
      {
        zodiacSign = day < 22 ? zod_signs[10] : zod_signs[11];
      }
      break;
    case 11:
      {
        zodiacSign = day < 22 ? zod_signs[11] : zod_signs[0];
      }
      break;
    default:
      {
        zodiacSign = "";
      }
      break;
  }

  return zodiacSign;
}
