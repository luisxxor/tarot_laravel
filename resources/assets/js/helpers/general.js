export function initialize(store, router) {
  const user = store.getters.currentUser;

  axios.defaults.headers.common["Content-Type"] = "application/json";

  if (user) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${
      store.getters.currentUser.token
    }`;
  }

  router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const currentUser = store.state.currentUser;

    if (requiresAuth && !currentUser) {
      if (to.path == "/admin/agendar" || to.path == "/admin/agendar/") {
        window.location.href = "/agendar";
      } else {
        next("/admin");
      }
    } else if ((to.path == "/admin" || to.path == "/admin/") && currentUser) {
      next("/admin/dashboard");
    } else if (
      (to.path == "/agendar" || to.path == "/agendar/") &&
      currentUser
    ) {
      window.location.href = "/admin/agendar/";
    } else {
      next();
    }
  });

  axios.interceptors.response.use(null, error => {
    if (error.response.status == 401) {
      store.commit("logout");

      router.push("/admin");
    }

    return Promise.reject(error);
  });
}
