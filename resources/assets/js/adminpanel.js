require("./bootstrap");
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import MainApp from "./components/admin/MainApp.vue";
import router from "./router/adminpanel.js";
import store from "./store";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);
import { initialize } from "./helpers/general";
initialize(store, router);
Vue.config.productionTip = true;
Vue.config.devtools = true;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  components: { MainApp }
});
