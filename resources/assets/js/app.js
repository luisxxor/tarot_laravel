// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import MainApp from "./components/MainApp.vue";
import MainPayment from "./components/MainPayment.vue";
import router from "./router";
import store from "./store";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import Vue2TouchEvents from "vue2-touch-events";

Vue.use(Vue2TouchEvents);

require("./bootstrap");

Vue.use(Vuetify);

import { initialize } from "./helpers/general";
initialize(store, router);
Vue.config.productionTip = true;
Vue.config.devtools = false;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  components: { MainApp, MainPayment }
});
