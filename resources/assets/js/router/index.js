import Vue from "vue";
import Router from "vue-router";
import Landing from "../components/Landing";
import Servicios from "../components/Servicios";
import Agendar from "../components/Agendar";
import Success from "../components/payments/Success";
import Status from "../components/payments/Status";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Landing",
      component: Landing
    },
    {
      path: "/servicios",
      name: "Servicios",
      component: Servicios
    },
    {
      path: "/agendar",
      name: "Agendar",
      component: Agendar
    },
    {
      path: "/payment_success",
      name: "Success",
      component: Success,
      props: true
    },
    {
      path: "/payment_status",
      name: "Status",
      component: Status,
      props: true
    }
  ]
});
