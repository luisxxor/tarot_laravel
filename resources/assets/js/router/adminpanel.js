import Vue from "vue";
import Router from "vue-router";
import Store from "../store";
import Login from "../components/admin/Login";
import Dashboard from "../components/admin/Dashboard";
import Servicios from "../components/admin/Servicios";
import Agendar from "../components/admin/Agendar";
import Disponibilidad from "../components/admin/Disponibilidad";
import Usuarios from "../components/admin/Usuarios";
import Citas from "../components/admin/Citas";
import ServiciosFront from "../components/admin/ServiciosFront";
import CambiarImagen from "../components/admin/CambiarImagen";
import Clientes from "../components/admin/Clientes.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  store: Store,
  routes: [
    {
      path: "/admin/",
      name: "Login",
      component: Login
    },
    {
      path: "/admin/dashboard",
      name: "Dashboard",
      component: Dashboard,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/servicios",
      name: "Servicios",
      component: Servicios,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/clientes",
      name: "Clientes",
      component: Clientes,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/agendar",
      name: "Agendar",
      component: Agendar,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/disponibilidad",
      name: "Disponibilidad",
      component: Disponibilidad,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/usuarios",
      name: "Usuarios",
      component: Usuarios,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/citas",
      name: "Citas",
      component: Citas,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/serviciosFront",
      name: "ServiciosFront",
      component: ServiciosFront,
      meta: { requiresAuth: true }
    },
    {
      path: "/admin/cambiarImagen",
      name: "CambiarImagen",
      component: CambiarImagen,
      meta: { requiresAuth: true }
    }
  ]
});
