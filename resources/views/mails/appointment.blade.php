Hola <i>{{ $appointmentCreated->receiver }}</i>.
@if($appointmentCreated->date)
<p>El cliente <b>{{ $appointmentCreated->client_name }}</b> ha agendado una cita para el dia <b>{{ $appointmentCreated->date }}</b> a las <b>{{ $appointmentCreated->turn }}</b> solicitando el servicio <b>{{ $appointmentCreated->service }}</b> bajo el id #<b>{{ $appointmentCreated->id }}</b>.</p>
@else
<p>El cliente <b>{{ $appointmentCreated->client_name }}</b> ha solicitado el servicio <b>{{ $appointmentCreated->service }}</b> bajo el id #<b>{{ $appointmentCreated->id }}</b>.</p>
@endif

@if($appointmentCreated->flowOrder)
    <p>El usuario ha hecho el pago a través de Flow y este es el FlowOrder de la operación: <b>{{$appointmentCreated->flowOrder}}</b>.</p>
@endif
 
Atentamente.<br/>
<i>{{ $appointmentCreated->sender }}</i>.