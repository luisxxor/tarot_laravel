<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname','email','description','tel','appointment_date','appointment_turn','service_id','client_id','payment_url','flowOrder','spell_date','spell_finished'
    ];

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
