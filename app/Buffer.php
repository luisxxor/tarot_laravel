<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buffer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'buffer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname','email','tel','appointment_date','appointment_turn','service_id','payment_url'
    ];
}
