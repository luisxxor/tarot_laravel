<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DefaultDate extends Model
{
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'weekday', 'day_data'
    ];
}
