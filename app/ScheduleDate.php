<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleDate extends Model
{
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'day_data'
    ];

    protected $primaryKey = 'date'; // or null

    public $incrementing = false;
}
