<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buffer;
use App\ScheduleDate;
use App\DefaultDate;
use App\Service;

class BufferController extends Controller
{

    public function getWeekDay($date)
	{
		$time = strtotime($date);
    	$day = date('D',$time);

	    switch ($day)
		{
		    case 'Mon':
		        $weekday = 0;
		        break;
		    case 'Tue':
		        $weekday = 1;
		        break;
		    case 'Wed':
		        $weekday = 2;
		        break;
		    case 'Thu':
				$weekday = 3;
		    	break;
		    case 'Fri':
				$weekday = 4;
		    	break;
		    case 'Sat':
				$weekday = 5;
		    	break;
		   	case 'Sun':
		   		$weekday = 6;
		   		break;
		}

		return $weekday;
    }

    public function is_available($date,$turn)
    {
		if($date == null && $turn == null)
		{
			return true;
		}

    	$schedule = ScheduleDate::where('date',$date)->first();

    	if($schedule!=null)
    	{
    		$day_data = $schedule->day_data;
    	}
    	else
    	{
    		$weekday = $this->getWeekDay($date);

			$day_data = DefaultDate::where('weekday',$weekday)
			->first()
			->day_data;
    	}

    	$parsed_day_data = json_decode($day_data,true);

		return $parsed_day_data[$turn];
	}
	
	public function index($id=null)
    {
        if($id==null)
        {
            return response()->json([
                'buffer' => Buffer::get()
            ]);
        }else{
            return response()->json([
                'buffer' => Buffer::find($id)
            ]);
        }
    }
    
    public function store(Request $request)
    {
		if(!Service::find($request->service_id)->agendable)
		{
			$date = null;
			$turn = null;
		}
		else
		{
			$date = $request->appointment_date;
			$turn = $request->appointment_turn;
		}

        if($this->is_available($date,$turn))
        {
            $buffer = new Buffer([
                'name' => $request->client_name,
				'lastname' => $request->client_lastname,
				'email' => $request->client_email,
				'tel' => $request->tel,
                'appointment_date' => $date,
                'appointment_turn' => $turn,
				'service_id' => $request->service_id,
				'payment_url' => $request->payment_url,
				'token' => $request->token_url
			]);
			
            $buffer->save();

            return response()->json([
                'buffer' => $buffer
            ], 201);
        }
        else
        {
            return response()->json([
                'error' => 'El turno no está disponible'
            ], 403);
        }
	}

	public function add_token(Request $request)
	{
		$buffer = Buffer::find($request->id);
		$buffer->token = $request->token;
		$buffer->save();

		return response()->json([
			'response' => 'done'
		],200);
	}

	public function destroy($id)
	{

		try {

			Buffer::destroy($id);
			
			return response()->json('Buffer Deleted',200);
		}
		catch (Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}
}
