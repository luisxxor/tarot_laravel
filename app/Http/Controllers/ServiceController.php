<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        if($id==null)
        {
            return response()->json([
                'services' => Service::get()
            ]);
        }else{
            return response()->json([
                'service' => Service::find($id)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select()
    {

        $services = [];
        foreach(Service::where('agendable')->get() as $service)
        {
            $services[] = ['value' => $service->id, 'text' => $service->title];
        }

        return response()->json([
            'services' => $services
        ]);
    }

    public function getNonAgendable()
    {
        $services = [];
        foreach(Service::where('agendable','=','0')->get() as $service)
        {
            $services[] = ['value' => $service->id, 'text' => $service->title];
        }

        return response()->json([
            'services' => $services
        ]);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ], [
            'title' => 'Debe especificar el nombre del servicio',
        ]);

        Service::create($request->all());

        return response()->json([
            'message' => 'Creado correctamente'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required'
        ], [
            'title' => 'Debe especificar el nombre del servicio',
        ]);

        Service::where('id', $id)->update($request->all());

        return response()->json([
            'message' => 'Actualizado correctamente'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {    	
        Service::destroy($id);
        
        return response()->json([
            'message' => 'Eliminado correctamente'
        ],204);
    }
}
