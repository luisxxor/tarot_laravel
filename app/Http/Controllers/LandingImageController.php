<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingImageController extends Controller
{
    public function update(Request $request)
    {

        if($request->get('image'))
        {
            $image = $request->get('image');
            \Image::make($request->get('image'))->save(public_path('img'.DIRECTORY_SEPARATOR.'a.jpg'));
        }

       return response()->json(['success' => 'La imagen ha sido actualizada correctamente'], 201);
    }
}
