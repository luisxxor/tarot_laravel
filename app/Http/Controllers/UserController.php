<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        if($id==null)
        {
            return response()->json([
                'users' => User::get()
            ]);
        }else{
            return response()->json([
                'user' => User::find($id)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
        ], [
            'email.required' => 'Debe especificar el email del usuario',
            'email.unique' => 'Ya existe un usuario registrado con ese email',
            'password.required' => 'Debe especificar la contraseña',
            'password.min' => 'La contraseña debe tener un minimo de 8 caracteres'
        ]);

        User::create(['email' => $request->email, 'password' => bcrypt($request->password)]);

        return response()->json([
            'message' => 'Creado correctamente'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password != '')
        {
            $request->validate([
                'email' => 'required',
                'password' => 'required|min:8',
            ], [
                'email.required' => 'Debe introducir el email del usuario',
                'password.required' => 'Debe introducir la contraseña',
                'password.min' => 'La contraseña debe tener un minimo de 8 caracteres'
            ]);
        }
        else {
            $request->validate([
                'email' => 'required'
            ], [
                'email.required' => 'Debe introducir el email del usuario'
            ]);
        }

        $data = ['email' => $request->email];

        if($request->password != '')
        {
            $data['password'] = bcrypt($request->password);
        }

        User::where('id', $id)->update($data);

        return response()->json([
            'message' => 'Actualizado correctamente'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {    	
        User::destroy($id);
        
        return response()->json([
            'message' => 'Eliminado correctamente'
        ],204);
    }

    public function usernameAvailable(Request $request)
    {
        if($request->id != null)
        {
            $available = !User::where('email',$request->email)->where('id','!=',$request->id)->exists();
        }
        else {
            $available =!User::where('email',$request->email)->exists();
        }
        return response()->json([
            'available' => $available
        ],200);
    }
}
