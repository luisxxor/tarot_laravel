<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FrontService;

class FrontServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        if($id == null)
        {
            return response()->json([
                'frontServices' => FrontService::orderBy('priority','id')->get()
            ]);
        }
        else
        {
            return response()->json([
                'frontService' => FrontService::find($id)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required'
        ], [
            'title' => 'Debe especificar el nombre del servicio',
        ]);

        if($request->get('image'))
        {
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('img'.DIRECTORY_SEPARATOR.'services'.DIRECTORY_SEPARATOR).$name);
        }

        $frontService= new FrontService();
        $frontService->title = $request->title;
        $frontService->image_name = $name;
        $frontService->description = $request->description;
        $frontService->priority = $request->priority;
        $frontService->save();

       return response()->json(['success' => 'El servicio ha sido creado correctamente'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required'
        ], [
            'title' => 'Debe especificar el nombre del servicio',
        ]);

        $frontService = FrontService::find($id);

        $image_path = public_path('img'.DIRECTORY_SEPARATOR.'services'.DIRECTORY_SEPARATOR.$frontService->image_name);

        if($request->get('image'))
        {
            if (\File::exists($image_path)) {
                \File::delete($image_path);
            }
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('img'.DIRECTORY_SEPARATOR.'services'.DIRECTORY_SEPARATOR).$name);
        }

        $frontService->title = $request->title;
        $frontService->description = $request->description;
        $frontService->image_name = $name;
        $frontService->priority = $request->priority;

        $frontService->save();

        return response()->json([
            'message' => 'Actualizado correctamente'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image_path = public_path('img'.DIRECTORY_SEPARATOR.'services'.DIRECTORY_SEPARATOR.FrontService::find($id)->image_name);

        if (\File::exists($image_path)) {
            \File::delete($image_path);
        }

        FrontService::destroy($id);
        
        return response()->json([
            'message' => 'Eliminado correctamente'
        ],204);
    }
}
