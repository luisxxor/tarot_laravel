<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DefaultDate;

class DefaultDateController extends Controller
{
    public function index()
    {
        return response()->json([
            'defaultDates' => DefaultDate::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        $default_dates = json_decode($request->data,true);
        
        foreach($default_dates as $default_date)
        {
            DB::table('default_dates')->where('weekday', $default_date['weekday'])->update(array('day_data' => json_encode($default_date['day_data'])));
        }

        return response()->json([
            'message' => 'Actualizado correctamente'
        ]);
    }
}
