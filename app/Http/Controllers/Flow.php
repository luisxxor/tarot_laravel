<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;

class Flow extends Controller
{

/*     private $url = 'https://www.flow.cl/api/';
    private $api_key = '23F64935-AC73-41BB-9595-1LD18ECE99A1';
    private $secret_key = 'a9793506dbe74375d2535458f50fc29c85fb27b1'; */

    private $url = 'https://sandbox.flow.cl/api/';
    private $api_key = '29FE20F8-4C3B-477A-804A-6EC807L1448C';
    private $secret_key = '75b429eb59fa21680f8cf3823397b79a37c2f7ef';

    private function sign($params)
    {
		$keys = array_keys($params);
		sort($keys);
		$toSign = "";
		foreach ($keys as $key) {
			$toSign .= "&" . $key . "=" . $params[$key];
		}
		$toSign = substr($toSign, 1);
		if(!function_exists("hash_hmac")) {
			throw new Exception("function hash_hmac not exist", 1);
		}
		return hash_hmac('sha256', $toSign , $this->secret_key);
	}

    private function getGETParamString($params)
    {
        $data = '';

        foreach($params as $key => $value)
        {
            $data.= '&'.rawurlencode($key).'='.rawurlencode($value);
        }

        return substr($data,1);
    }

    private function getPOSTParamString($params)
    {
        $data = '';

        foreach($params as $key => $value)
        {
            $data.= '&'.$key.'='.$value;
        }

        return substr($data,1);
    }
  

    private function postRequest($data,$url,$sign)
    {
        try
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data . "&s=" . $sign);
            $resp = curl_exec($ch);
            if($resp === false)
            {
                $err = curl_error($ch);
            } 
            $info = curl_getinfo($ch);
            if(!in_array($info['http_code'], array('200', '400', '401')))
            {
                $err = $info['http_code'];
            }
            if(isset($err))
            {
                return response()->json(['error'=>$err]);
            }
            else{
                return response()->json(['payment' => $resp]);
            }
        }catch (Exception $e) {
            return response()->json(['error'=> 'Error: ' . $e->getCode() . ' - ' . $e->getMessage()]);
        }
    }

    private function getRequest($data,$url,$sign)
    {
        try
        {
            $url = $url.'?'.$data.'&s='.$sign;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $resp = curl_exec($ch);
            if($resp === false)
            {
                $err = curl_error($ch);
            } 
            $info = curl_getinfo($ch);
            if(!in_array($info['http_code'], array('200', '400', '401')))
            {
                $err = $info['http_code'];
            }
            if(isset($err))
            {
                return ['error'=>$err];
            }
            else{
                return ['payment' => $resp];
            }
        }catch (Exception $e) {
            return ['error'=> 'Error: ' . $e->getCode() . ' - ' . $e->getMessage()];
        }
    }

    public function generatePayment(Request $request)
    {
        $params = array(
            'apiKey' => $this->api_key,
            'commerceOrder' => $request->id,
            'subject' => 'prueba',
            'currency' => 'CLP',
            'amount' => $request->price,
            'email' => $request->email,
            'urlConfirmation' => url('payment_confirm'),
            'urlReturn' => url('payment_status'),
            'paymentMethod' => $request->payment_method
        );

        ksort($params);

        $data = $this->getPOSTParamString($params);

        $payment_url = $this->url.'payment/create';
       
        $sign = $this->sign($params);

        return $this->postRequest($data,$payment_url,$sign);
    }

    public function confirmPayment(Request $request)
    {
        $params = array(
            'apiKey' => $this->api_key,
            'token' => $request->input('token')
        );

        ksort($params);

        $data = $this->getGETParamString($params);
        $sign = $this->sign($params);
        $payment_url = $this->url.'payment/getStatus';
        $response = $this->getRequest($data,$payment_url,$sign);

        return view('confirm',['payment' => $response]);
    }

    
    public function getPaymentStatus(Request $request)
    {
        $params = array(
            'apiKey' => $this->api_key,
            'token' => $request->input('token')
        );

        ksort($params);

        $data = $this->getGETParamString($params);

        $sign = $this->sign($params);

        $payment_url = $this->url.'payment/getStatus';

        $payment_data = $this->getRequest($data,$payment_url,$sign);
        if(json_decode($payment_data['payment'])->status == 2){
            return view('payment_success',['payment_data' => $payment_data['payment'], 'token' => $request->input('token')]);
        } else {
            return view('payment_error',['payment_data' => $payment_data['payment'], 'token' => $request->input('token')]);
        }
    }
}
