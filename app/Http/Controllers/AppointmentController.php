<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScheduleDate;
use App\DefaultDate;
use App\Appointment;
use App\Service;
use App\Client;
use Illuminate\Support\Facades\DB;
use App\Mail\AppointmentCreated;
use Illuminate\Support\Facades\Mail;

class AppointmentController extends Controller
{

	public function index($id=null)
	{
		if($id==null)
        {
            return response()->json([
                'appointments' => Appointment::with('service')->with('client')->get()
            ]);
        }else{
            return response()->json([
                'appointment' => Appointment::with('service')->with('client')->find($id)
            ]);
        }
	}

	public function getToday()
	{
		$timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
		$dt->setTimestamp($timestamp);

		$total = Appointment::whereHas('service',function ($query) {
			$query->where('agendable',1);
		})->where('appointment_date',$dt->format('Y-m-d'))->get();

		$hours = intval($dt->format('H'));
		$minutes = intval($dt->format('i'));
		$actual_turn = $hours * 2 + ($minutes < 30 ? 1 : 2);

		$pending = $total->filter(function($value) use ($actual_turn) { return $value->appointment_turn >= $actual_turn; })->count();
		$finished = $total->filter(function($value) use ($actual_turn) { return $value->appointment_turn < $actual_turn; })->count();

		return response()->json([
			'total' => $total->count(),
			'pending' => $pending,
			'finished' => $finished
		]);
	}

	public function resume()
	{
		$timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
		$dt->setTimestamp($timestamp);

		$hours = intval($dt->format('H'));
		$minutes = intval($dt->format('i'));

		$index = $hours * 2 + ($minutes < 30 ? 1 : 2);
		$today = $dt->format('Y-m-d');

		$lastAppointments = DB::table('appointments')
		->leftjoin('clients','appointments.client_id','=','clients.id')
		->leftjoin('services','appointments.service_id','=','services.id')
		->selectRaw("appointments.id, appointments.client_id, concat(clients.name,' ',coalesce(clients.lastname,'')) as full_name, appointment_date, appointment_turn, services.title, clients.tel")
		->where('appointment_date','>=',$today)
		->whereRaw('!((appointment_date = ?) AND (appointment_turn < ?))',[$today,$index])
		->orderBy('appointment_date','asc')->orderBy('appointment_turn','asc')
		->get();
	
		foreach($lastAppointments as $appointment)
		{
			$formatted_date = explode('-',$appointment->appointment_date);
			$formatted_date = implode('/',[$formatted_date[2],$formatted_date[1],$formatted_date[0]]);
			$appointment->appointment_date = $formatted_date;
			
			$hour = intdiv(intval($appointment->appointment_turn),2);
			$hminutes = intval($appointment->appointment_turn) % 2 == 1 ? '30' : '00';
			$htime = $hour < 10 ? '0'.$hour : ''.$hour;
			$htime.= ':'.$hminutes;

			$appointment->appointment_turn = $htime;

		}

		return response()->json([
			'appointments' => $lastAppointments
		]);
	}

	public function nextSpells()
	{
		$timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
		$dt->setTimestamp($timestamp);

		$today = $dt->format('Y-m-d');

		$lastSpells = DB::table('appointments')
		->leftjoin('clients','appointments.client_id','=','clients.id')
		->leftjoin('services','appointments.service_id','=','services.id')
		->selectRaw("appointments.id, appointments.client_id, concat(clients.name,' ',coalesce(clients.lastname,'')) as full_name, spell_date, spell_finished, services.title, clients.tel")
		->where('spell_finished','0')
		->orderBy('spell_date','asc')
		->get();

		return response()->json([
			'appointments' => $lastSpells
		]);
	}

	public function getStartTimeFromIndex($index)
	{
		if(($index>48)||($index<0)){
			return false;
		}

		if($index == 48)
		{
			return '00:00';
		}

		$hours = strval(floor($index/2));
		if(strlen($hours) == 1){
			$hours = '0'.$hours;
		}

		$minutes = ($index%2 == 0) ? '00' : '30'; 

		return $hours.':'.$minutes;
	}

	public function getIndexFromStartTime($time)
	{
		$arrtime = explode(":",$time);
		$hour = $arrtime[0];
		$minutes = $arrtime[1];

		return $hour * 2 + ($minutes == '30' ? 1 : 0);
	}

	public function getWeekDay($date)
	{
		$timestamp = strtotime($date);
		$dt = new \DateTime("now");
		$dt->setTimestamp($timestamp);
		$weekday = intval($dt->format('N')) - 1;

		return $weekday;
	}

    public function available($date)
    {
    	$schedule = ScheduleDate::find($date);

		$timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
		$dt->setTimestamp($timestamp);

    	if($schedule!=null)
    	{		
			$day_data = $schedule->day_data;

			if($date == $dt->format('Y-m-d'))
			{
				$hour = intval($dt->format('H'));
				$minutes = intval($dt->format('i'));

				if($minutes >= 30)
				{
					$minutes = 0;
					$hour+=1;
				}
				else {
					$minutes = 30;
				}

				$index = $hour * 2 + ($minutes == 30 ? 1 : 0 );

				$ps = json_decode($day_data,true);

				for($i=0;$i<$index;$i++)
				{
					$ps[$i] = false;
				}

				$day_data = json_encode($ps);
			}

    		$day_data = $schedule->day_data;
    	}
    	else
    	{
    		$weekday = $this->getWeekDay($date);

			$day_data = DefaultDate::where('weekday',$weekday)
			->first()
			->day_data;
    	}

		if($date == $dt->format('Y-m-d'))
		{
			$hour = intval($dt->format('H'));
			$minutes = intval($dt->format('i'));

			if($minutes >= 30)
			{
				$minutes = 0;
				$hour+=1;
			}
			else {
				$minutes = 30;
			}

			$index = $hour * 2 + ($minutes == 30 ? 1 : 0 );
		}
		else
		{
			$index = 0;
		}

		$parsed_day_data_unfiltered = json_decode($day_data,true);
		
		$parsed_day_data = array_map(function($v,$k) use ($index) {if(($v == true)){ return $k >= $index; }else{ return false; }},$parsed_day_data_unfiltered,array_keys($parsed_day_data_unfiltered));

		$text = array_filter(array_map(function($v,$k){if($v){return $this->getStartTimeFromIndex($k)." - ".$this->getStartTimeFromIndex($k+1);}}, $parsed_day_data, array_keys($parsed_day_data)));
	
		$available = [];

		foreach($text as $key => $value)
		{
			$initial_hour = substr($value,0,5);
			$available[] = array('value' => $this->getIndexFromStartTime($initial_hour),'text' => $value);				
		}

        return response()->json([
            'available' => $available
        ]);
    }

    public function adminAvailable($date)
    {
		$timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
		$dt->setTimestamp($timestamp);

		$day_data = array_fill(0,48,true);

		$text = array_filter(array_map(function($v,$k){if($v){return $this->getStartTimeFromIndex($k)." - ".$this->getStartTimeFromIndex($k+1);}}, $day_data, array_keys($day_data)));
		
		$available = [];

		foreach($text as $key => $value)
		{
			$initial_hour = substr($value,0,5);
			$available[] = array('value' => $this->getIndexFromStartTime($initial_hour),'text' => $value);				
		}

        return response()->json([
            'available' => $available
        ]);
	}
	
	public function adminSelectAvailable($date, $selected_turn = null)
	{
		$day_data = array_fill(0,48,true);
		$appointments = Appointment::where('appointment_date',$date)->get();

		if(count($appointments) > 0)
		{
			foreach($appointments as $appointment)
			{
				$day_data[$appointment->appointment_turn] = false;
			}
		}

		if($selected_turn != null)
		{
			$day_data[$selected_turn] = true;
		}

		$text = array_filter(array_map(function($v,$k){if($v){return $this->getStartTimeFromIndex($k)." - ".$this->getStartTimeFromIndex($k+1);}}, $day_data, array_keys($day_data)));
		
		$available = [];

		foreach($text as $key => $value)
		{
			$initial_hour = substr($value,0,5);
			$available[] = array('value' => $this->getIndexFromStartTime($initial_hour),'text' => $value);				
		}

        return response()->json([
            'available' => $available
        ]);		
	}

	public function store(Request $request)
    {

		$timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
		$dt->setTimestamp($timestamp);

     	$request->validate([
    		'client_name' => 'required',
    		'appointment_date' => 'nullable|date|date_format:Y-m-d|after_or_equal:'.$dt->format('Y-m-d'),
    		'appointment_turn' => 'nullable|numeric',
			'service_id' => 'required|exists:services,id',
			'client_email' => 'required|email'
    	],[
    		'client_name.required' => 'Debe introducir el nombre',
    		'appointment_date.date_format' => 'El formato de la fecha no es válido',
    		'appointment_date.after_or_equal' => 'Ésta fecha ya pasó',
    		'appointment_turn.numeric' => 'El formato del turno no es válido',
    		'service_id.required' => 'Debe seleccionar un servicio',
			'service_id.exists' => 'Éste servicio no existe',
			'client_email.required' => 'Debe introducir un email',
			'client_email.email' => 'El email debe ser válido'
		]);


		if(Service::find($request->service_id)->agendable)
		{
			if(ScheduleDate::find($request->appointment_date)==null)
			{
				$day_data = json_decode(DefaultDate::where('weekday',$this->getWeekDay($request->appointment_date))->first()->day_data,true);
				if($day_data[$request->appointment_turn])
				{
					$day_data[$request->appointment_turn] = false;
				}
				else
				{
					return response()->json([
						'appointment' => 'El turno seleccionado no está disponible',
						'code' => 410
					], 410);
				}

				ScheduleDate::create([
					'date' => $request->appointment_date,
					'day_data' => json_encode($day_data)
				]);
			}
			else
			{
				$schedule = ScheduleDate::find($request->appointment_date);
				$day_data = json_decode($schedule->day_data,true);
				
				if($day_data[$request->appointment_turn])
				{
					$day_data[$request->appointment_turn] = false;
				}
				else
				{
					return response()->json([
						'appointment' => 'sEl turno seleccionado no está disponible'
					], 410);
				}

				$schedule->day_data = json_encode($day_data);
				$schedule->save();
			}
		}

		$client = new Client([
			'name' => $request->client_name,
			'lastname' => $request->client_lastname,
			'email' => $request->client_email,
			'tel' => $request->tel,
			'city' => $request->city,
			'birthdate' => $request->birthdate
		]);

		$client->save();

    	$appointment = new Appointment([
			'client_id' => $client->id,
    		'appointment_date' => $request->appointment_date,
    		'appointment_turn' => $request->appointment_turn,
			'service_id' => $request->service_id,
			'flowOrder' => $request->flowOrder
    	]);
		
		$appointment->save();
		
    	return response()->json([
            'appointment' => $appointment
        ], 201);
	}

	public function adminStore(Request $request)
    {
		$timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
		$dt->setTimestamp($timestamp);

     	$request->validate([
    		'appointment_date' => 'nullable|date|date_format:Y-m-d',
    		'appointment_turn' => 'nullable|numeric',
			'service_id' => 'required|exists:services,id'
    	],[
    		'appointment_date.date_format' => 'El formato de la fecha no es válido',
    		'appointment_turn.numeric' => 'El formato del turno no es válido',
    		'service_id.required' => 'Debe seleccionar un servicio',
			'service_id.exists' => 'Éste servicio no existe',
    	]);

		$apdate = null;
		$apturn = null;

		if(Service::find($request->service_id)->agendable)
		{
			$apdate = $request->appointment_date;
			$apturn = $request->appointment_turn;
			$spell_date = NULL;
			$spell_finished = false;
			if(ScheduleDate::find($request->appointment_date)==null)
			{

				$day_data = [];

				for($i=0;$i<48;$i++)
				{
					$day_data[] = true;
				}

				$day_data[$request->appointment_turn] = false;

				ScheduleDate::create([
					'date' => $request->appointment_date,
					'day_data' => json_encode($day_data)
				]);
			}
			else
			{
				$schedule = ScheduleDate::find($request->appointment_date);
				$day_data = json_decode($schedule->day_data,true);
				$appointment = Appointment::where('appointment_date',$request->appointment_date)->where('appointment_turn',$request->appoinment_turn);

				$schedule->day_data = json_encode($day_data);
				$schedule->save();
			}
		}
		else
		{
			$spell_date = $request->spell_date;
			$spell_finished = $request->spell_finished;
		}

		if($request->newClient)
		{
			$client = new Client([
				'name' => $request->client_name,
				'lastname' => $request->client_lastname,
				'email' => $request->client_email,
				'tel' => $request->tel,
				'city' => $request->city,
				'birthdate' => $request->birthdate
			]);

			$client->save();
		}
		else
		{
			$client = Client::find($request->client_selected);
		}

    	$appointment = new Appointment([
			'client_id' => $client->id,
    		'appointment_date' => $apdate,
    		'appointment_turn' => $apturn,
			'service_id' => $request->service_id,
			'spell_date' => $spell_date,
			'spell_finished' => $spell_finished
    	]);

		$appointment->save();
		 
    	return response()->json([
            'appointment' => $appointment
        ], 201);
	}
	
	public function update(Request $request)
	{
		$request->validate([
			'id' => 'required,exists:appointment,id',
    		'appointment_date' => 'nullable|date|date_format:Y-m-d',
    		'appointment_turn' => 'nullable|numeric',
			'service_id' => 'required|exists:services,id',
			'client_email' => 'nullable|email'
    	],[
    		'client_name.required' => 'Debe introducir el nombre',
    		'appointment_turn.numeric' => 'El formato del turno no es válido',
    		'service_id.required' => 'Debe seleccionar un servicio',
			'service_id.exists' => 'Éste servicio no existe',
			'client_email.email' => 'El email debe ser válido',
			'id.required' => 'Debe seleccionar un id válido',
			'id.exists' => 'Debe ser una cita existente'
		]);

		$apdate = null;
		$apturn = null;
		$appointment = Appointment::find($request->id);

		if(Service::find($request->service_id)->agendable)
		{
			$spell_date = NULL;
			$spell_finished = false;
			$existsAnAppointmentInRequestedTime = Appointment::where('appointment_date',$request->appointment_date)->where('appointment_turn',$request->appointment_turn)->where('id','!=',$request->id)->first();
			
			if($existsAnAppointmentInRequestedTime)
			{
				return response()->json([
					'appointment' => 'El turno seleccionado no está disponible'
				], 410);
			}
	
			if(Appointment::find($request->id)->service->agendable)
			{
				$scheduleAntiguo = ScheduleDate::find($appointment->appointment_date);
		
				$dayDataAntiguo = json_decode($scheduleAntiguo->day_data,true);
				
				$dayDataAntiguo[$appointment->appointment_turn] = true;
		
				$scheduleAntiguo->day_data = json_encode($dayDataAntiguo);
		
				$scheduleAntiguo->save();
			}
	
			$schedule = ScheduleDate::find($request->appointment_date);
	
			if($schedule)
			{
				$dayData = json_decode($schedule->day_data,true);
				$dayData[$request->appointment_turn] = false;
				$schedule->day_data = json_encode($dayData);
				$schedule->save();
			}
			else
			{
				$dayData = json_decode(DefaultDate::where('weekday',$this->getWeekDay($request->appointment_date))->first()->day_data,true);
				$dayData[$request->appointment_turn] = false;
	
				$schedule = new ScheduleDate([
					'date' => $request->appointment_date,
					'day_data' => json_encode($dayData)
				]);
	
				$schedule->save();
			}

			$apdate = $request->appointment_date;
			$apturn = $request->appointment_turn;
		}
		else if(Appointment::find($request->id)->service->agendable)
		{
			$scheduleAntiguo = ScheduleDate::find($appointment->appointment_date);

			$dayDataAntiguo = json_decode($scheduleAntiguo->day_data,true);
			
			$dayDataAntiguo[$appointment->appointment_turn] = true;

			$scheduleAntiguo->day_data = json_encode($dayDataAntiguo);

			$scheduleAntiguo->save();
			
			$spell_date = $request->spell_date;
			$spell_finished = $request->spell_finished;
		}
		else
		{
			$spell_date = $request->spell_date;
			$spell_finished = $request->spell_finished;
		}

		if($request->newClient)
		{
			$client = new Client([
				'name' => $request->client_name,
				'lastname' => $request->client_lastname,
				'email' => $request->client_email,
				'tel' => $request->tel
			]);

			$client->save();
		}
		else
		{
			$client = Client::find($request->client_selected);
		}

		$appointment->client_id = $client->id;
		$appointment->appointment_date = $apdate;
		$appointment->appointment_turn = $apturn;
		$appointment->service_id = $request->service_id;
		$appointment->spell_date = $spell_date;
		$appointment->spell_finished = $spell_finished;

		$appointment->save();

		return response()->json([
			'appointment' => $appointment
		], 200);
	}

	public function onCreateSendMail(Request $request)
	{

 		$appointment = Appointment::find($request->appointment_id);

		if($appointment != null)
		{
			$objAppointment = new \stdClass();
			$objAppointment->client_name = $appointment->lastname ? $appointment->name : $appointment->name.' '.$appointment->lastname;
			$dt = new \DateTime($appointment->appointment_date, new \DateTimeZone('America/Santiago'));
			$objAppointment->date = $dt->format('d/m/Y');
			$objAppointment->turn = $this->getStartTimeFromIndex($appointment->appointment_turn);
			$objAppointment->service = $appointment->service->title;
			$objAppointment->id = $appointment->id;
			$objAppointment->flowOrder = null;
			$objAppointment->sender = 'Tarotjanet.cl Admin';
			$objAppointment->receiver = 'Janet';
			
			Mail::to("jan.orellanasal@gmail.com")->send( new AppointmentCreated($objAppointment) );
			return response()->json([
				'message' => 'good'
			]);
		}
		else
		{
			return response()->json([
				'message' => 'no existe esa cita'
			]);
		}
	}

	public function destroy($id)
    {
		$appointment = Appointment::find($id);

		if($appointment->service->agendable)
		{
			$schedule = ScheduleDate::find($appointment->appointment_date);
			
			$day_data = json_decode($schedule->day_data,true);
			$day_data[$appointment->appointment_turn] = true;
			$schedule->day_data = json_encode($day_data);
		}

        $appointment->delete();
        
        return response()->json([
            'message' => 'Eliminado correctamente'
        ],204);
    }
}
