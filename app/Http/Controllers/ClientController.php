<?php

namespace App\Http\Controllers;

use App\Client;
use App\Problem;
use App\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=NULL)
    {
        if($id)
        {
            return response()->json([
                'client' => Client::with(['appointments' => function ($app) { return $app->whereHas('service',function ($query) { $query->where('agendable',0); }); },'problems'])->find($id)
            ]);
        }else{
            return response()->json([
                'clients' => Client::with(['appointments' => function ($app) { return $app->whereHas('service',function ($query) { $query->where('agendable',0); }); },'problems'])->get()
            ]);
        }
    }

    public function select()
    {

        $clients = [];
        foreach(Client::get() as $client)
        {
            $birthdate = '';
            if($client->birthdate!=null)
            {
                $birthdate = ' - '.implode('/',array_reverse(explode('-',$client->birthdate)));
            }
            $clients[] = ['value' => $client->id, 'text' => $client->name." ".$client->lastname.$birthdate];
        }

        return response()->json([
            'clients' => $clients
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
        $dt->setTimestamp($timestamp);
        
        $request->validate([
    		'name' => 'required',
    		'lastname' => 'required',
            'birthdate' => 'required|date|date_format:Y-m-d|before_or_equal:'.$dt->format('Y-m-d'),
            'girls' => 'nullable|numeric|min:0',
            'boys' => 'nullable|numeric|min:0'
    	],[
    		'name.required' => 'Debe introducir el nombre',
            'birthdate.required' => 'La fecha de nacimiento es obligatoria',
            'birthdate.date_format' => 'El formato de la fecha no es válido',
    		'birthdate.before_or_equal' => 'Esta fecha es inválida',
            'girls.numeric' => 'Este campo tiene que ser un número válido',
            'girls.min' => 'El número no debe ser negativo',
            'boys.numeric' => 'Este campo tiene que ser un número válido',
            'boys.min' => 'El número no debe ser negativo',
        ]);

        $client = new Client();

        $client->name = $request->name;
        $client->lastname = $request->lastname;
        $client->birthdate = $request->birthdate;
        $client->girls = $request->girls;
        $client->boys = $request->boys;
        $client->city = $request->city;
        $client->email = $request->email;

        $client->save();

        $problems = [];
        $appointments = [];

        if(count($request->problems)>0)
        {
            foreach($request->problems as $problem)
            {
                $problems[] = ['client_id' => $client->id, 'date' => $problem['date'], 'description' => $problem['description']];
            }
            Problem::where('client_id',$client->id)->delete();
            Problem::insert($problems);
        }

        if(count($request->appointments)>0)
        {
            foreach($request->appointments as $appointment)
            {
                $appointments[] = $appointment;
            }
            DB::table('appointments')->join('services','appointments.service_id','services.id')->where('agendable',0)->delete();
            Appointment::insert($appointments);
        }

        return response()->json([
            'client' => $client,
            'appointments' => $appointments
        ],201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $timestamp = time();
		$dt = new \DateTime("now",new \DateTimeZone('America/Santiago'));
        $dt->setTimestamp($timestamp);
        
        $request->validate([
    		'name' => 'required',
    		'lastname' => 'required',
            'birthdate' => 'required|date|date_format:Y-m-d|before_or_equal:'.$dt->format('Y-m-d'),
            'girls' => 'nullable|numeric|min:0',
            'boys' => 'nullable|numeric|min:0'
    	],[
    		'name.required' => 'Debe introducir el nombre',
            'birthdate.required' => 'La fecha de nacimiento es obligatoria',
            'birthdate.date_format' => 'El formato de la fecha no es válido',
    		'birthdate.before_or_equal' => 'Esta fecha es inválida',
            'girls.numeric' => 'Este campo tiene que ser un número válido',
            'girls.min' => 'El número no debe ser negativo',
            'boys.numeric' => 'Este campo tiene que ser un número válido',
            'boys.min' => 'El número no debe ser negativo',
        ]);

        $client = Client::find($id);

        $client->name = $request->name;
        $client->lastname = $request->lastname;
        $client->birthdate = $request->birthdate;
        $client->girls = $request->girls;
        $client->boys = $request->boys;
        $client->city = $request->city;
        $client->email = $request->email;

        $client->save();

        $problems = [];
        $appointments = [];

        Problem::where('client_id',$client->id)->delete();
        if(count($request->problems)>0)
        {
            foreach($request->problems as $problem)
            {
                $problems[] = ['client_id' => $client->id, 'date' => $problem['date'], 'description' => $problem['description']];
            }
            Problem::insert($problems);
        }

        DB::table('appointments')->join('services','appointments.service_id','services.id')->where('agendable',0)->delete();
        if(count($request->appointments)>0)
        {
            foreach($request->appointments as $appointment)
            {
                $appointments[] = ['client_id' => $client->id, 'spell_date' => $appointment['spell_finished'] ? $appointment['spell_date'] : NULL, 'service_id' => $appointment['service_id'], 'spell_finished' => $appointment['spell_finished']];
            }
            Appointment::insert($appointments);
        }

        return response()->json([
            'client' => $client,
            'appointments' => $appointments
        ],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        Problem::where('client_id',$client->id)->delete();
        DB::table('appointments')->join('services','appointments.service_id','services.id')->where('agendable',0)->delete();
        $client->delete();

        return response()->json([
            'message' => 'Eliminado correctamente'
        ],204);
    }
}