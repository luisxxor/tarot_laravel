<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'lastname','email','tel','birthdate','boys','girls','city'
    ];

    public function problems()
    {
        return $this->hasMany('App\Problem');
    }

    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }
}
