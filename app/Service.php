<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'price', 'agendable'
    ];

    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }
}
