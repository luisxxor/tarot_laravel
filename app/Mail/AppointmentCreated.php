<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointmentCreated extends Mailable
{
    use Queueable, SerializesModels;

/**
     * The appointment object instance.
     *
     * @var AppointmentCreated
     */
    public $appointmentCreated;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $appointmentCreated )
    {
        $this->appointmentCreated = $appointmentCreated; 
        $this->subject('¡Nuevo agendamiento!');
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('admin@tarotjanet.cl')
                    ->view('mails.appointment');
    }
}
